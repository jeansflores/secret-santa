class Api::V1::CampaignsController < Api::V1::BaseController
  def index
    campaigns = Campaign.all
    render json: campaigns, include: :members
  end

  def show
    campaign = Campaign.find(params[:id])
    render json: campaign, include: :members
  end
end
