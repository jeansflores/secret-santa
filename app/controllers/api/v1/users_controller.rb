class Api::V1::UsersController < Api::V1::BaseController
  def index
    users = User.all
    render json: users, include: { campaigns: { include: :members } }
  end

  def show
    user = User.find(params[:id])
    render json: user, include: { campaigns: { include: :members } }
  end
end
