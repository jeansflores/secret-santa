class Api::V1::MembersController < Api::V1::BaseController
  def index
    members = Member.all
    render json: members
  end

  def show
    member = Member.find(params[:id])
    render json: member
  end
end
