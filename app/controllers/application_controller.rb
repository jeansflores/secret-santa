class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  rescue_from ActiveRecord::RecordNotFound, :with => :render_404

  def render_404
    # TODO Create page 404
    redirect_to '/404'
    # redirect_to main_app.root_url
  end
end
