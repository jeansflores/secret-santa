require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :users, :controllers => { registrations: 'registrations' }

  mount Sidekiq::Web => '/sidekiq'

  root to: 'pages#home'

  resources :campaigns, except: [:new] do
    post 'raffle', on: :member
    # post 'raffle', on: :collection
  end

  resources :members, only: [:create, :destroy, :update]

  get 'members/:token/opened', to: 'members#opened'

  namespace :api do
    namespace :v1 do
      resources :users, only: [:index, :create, :show, :update, :destroy]
      resources :campaigns, only: [:index, :create, :show, :update, :destroy]
      resources :members, only: [:index, :create, :show, :update, :destroy]
    end
  end
end
