require 'rails_helper'

feature 'Users' do
  scenario 'filter/search' do
    pending ""
    campaing = FactoryGirl.cache(:campaing)
    member = FactoryGirl.cache(:member)

    fisica = FactoryGirl.cache(:fisica, member: member)
    brunno = FactoryGirl.cache(:brunno, member: member)

    seller = FactoryGirl.cache(:seller, groups: [campaing], partner: brunno)
    douglas = FactoryGirl.cache(:user_douglas, groups: [campaing], partner: fisica)

    FactoryGirl.cache(:event_monitoring, user: seller)
    FactoryGirl.cache(:event_monitoring, user: douglas)

    navigate 'Administração > Monitoramento de Eventos'

    filter({ name: 'Aprovação de Processos' })

    within_table_list do
      expect(page).to have_css 'tbody tr', count: 2

      expect(page).to have_content 'BRUNNO'
      expect(page).to have_content 'PESSOA FISICA'
    end

    search('Usuário', 'BRUNNO')

    within_table_list do
      expect(page).to have_css 'tbody tr', count: 1

      expect(page).to have_content 'BRUNNO'
      expect(page).to_not have_content 'PESSOA FISICA'
    end

    filter({ name: 'Importação de Linhas' })

    within_table_list { expect(page).to have_css 'tbody tr', count: 0 }
  end

  scenario 'create/destroy event monitoring' do
    pending ""
    campaing = FactoryGirl.cache(:campaing)
    member = FactoryGirl.cache(:member)

    fisica = FactoryGirl.cache(:fisica, member: member)
    brunno = FactoryGirl.cache(:brunno, member: member)
    tiago = FactoryGirl.cache(:tiago, member: member)

    FactoryGirl.cache(:seller, groups: [campaing], partner: brunno)
    FactoryGirl.cache(:user_douglas, groups: [campaing], partner: fisica)
    FactoryGirl.cache(:test, groups: [campaing], partner: tiago, active: false)
    FactoryGirl.cache(:jack)

    navigate 'Administração > Monitoramento de Eventos'

    click_link 'Novo'

    select 'Aprovação de Processos', from: 'Evento'
    check_options_from_select('event_monitoring_user_id', ['PESSOA JURIDICA', 'TIAGO'], false)
    select 'BRUNNO', from: 'Usuário'

    click_button 'Salvar'

    expect(page).to have_notice 'Monitoramento de Eventos criado com sucesso.'

    within_table_list do
      expect(page).to have_css 'tbody tr', count: 1

      expect(page).to have_content 'Aprovação de Processos'
      expect(page).to have_content 'BRUNNO'
    end

    click_link 'Novo'

    select 'Aprovação de Processos', from: 'Evento'
    select 'BRUNNO', from: 'Usuário'

    click_button 'Salvar'

    expect(page).to have_alert 'Monitoramento de Eventos não pode ser criado.'
    expect(page).to have_content 'já está em uso'

    select 'PESSOA FISICA', from: 'Usuário'

    click_button 'Salvar'

    expect(page).to have_notice 'Monitoramento de Eventos criado com sucesso.'

    within_table_list do
      expect(page).to have_css 'tbody tr', count: 2

      expect(page).to have_content 'Aprovação de Processos'
      expect(page).to have_content 'BRUNNO'
      expect(page).to have_content 'PESSOA FISICA'
    end

    click_link 'Deletar', confirm: true

    expect(page).to have_notice 'Monitoramento de Eventos deletado com sucesso.'

    within_table_list do
      expect(page).to have_css 'tbody tr', count: 1

      expect(page).to have_content 'Aprovação de Processos'
      expect(page).to have_content 'PESSOA FISICA'
    end
  end
end
